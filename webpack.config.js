let path = require('path');
const HtmlWebPackPlugin = require('html-webpack-plugin');
module.exports = {
    entry: './src/index.tsx',
    output: {
      filename: '[name].[hash].js',
      path: path.resolve(__dirname, 'build')
    },
    module: {
      rules: [
            {
                test: /\.tsx$/,
                exclude: /node_modules/,
                loaders: ["ts-loader"]
            },
            {
              test: /\.css$/,
              loaders:["style-loader", "css-loader" ]
             
          },
          {
              test: /\.scss$/,
              loaders:["style-loader", "css-loader", "scss-loader"]
              
          }
        ]
    },
    resolve: {
        extensions: [ '.tsx', '.ts', '.js' ]
      },
    plugins: [
      new HtmlWebPackPlugin({
        template: './src/index.html'
      })
    ],
    devtool: 'inline-source-map',
    devServer: {
        contentBase: "./build",
        hot: true
    },
  }

